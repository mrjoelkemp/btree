var root = this;

if (typeof exports !== 'undefined') {
  root = exports;
}

root.Tree = function Tree (left, right, val) {
  var isValidTree = function (t) {
    return t && t.getLeftChild && t.getRightChild && t.getValue;
  };

  return {
    left:   isValidTree(left) ? left : null,
    right:  isValidTree(right) ? right : null,
    value:  val || null,

    getValue: function () {
      return this.value;
    },

    setValue: function (val) {
      this.value = val;
    },

    getLeftChild: function () {
      return this.left;
    },

    getRightChild: function () {
      return this.right;
    },

    setLeftChild: function (child) {
      if (! isValidTree(child)) throw 'Not a valid tree';

      this.left = child;
    },

    setRightChild: function (child) {
      if (! isValidTree(child)) throw 'Not a valid tree';

      this.right = child;
    },

    // A tree is a leaf if it has no children®
    isLeaf: function () {
      return this.left === null && this.right === null;
    },

    // The current tree is equal to another tree
    // if the children and value are the same
    equals: function (node) {

      if (isValidTree(node)) {

        // If we at leaves
        if (this === node) return true;

        // Mismatch of children
        if (! this.left  && node.left || ! this.right && node.right) return false;

        // Two trees are equal if their values are equal and
        // every child is equal
        return  this.value === node.value &&
                (this.left === node.left || this.left.equals(node.left)) &&
                (this.right === node.right || this.right.equals(node.right));

      }

      return false;
    },

    // Override for pretty printing
    valueOf: function () {
      var left = {}, right= {},
          value = {
            value: this.value,
            children: []
          };

      if (this.right) value.children.push(this.right.valueOf());
      if (this.left) value.children.push(this.left.valueOf());

      return value;
    },

    // Returns a reference to the first tree with the passed value
    // unless 'all' is specified in which case a list of trees are returned
    // TODO: Highlight the current node within the svt
    find: function (val, all, results) {
      results = results || [];

      if (this.value === val) {
        if (all) {
          results.push(this);
        } else {
          return this;
        }
      }

      if (! this.left && ! this.right) return all ? results : null;

      this.find.apply(this.left, [val, all, results]);
      this.find.apply(this.right, [val, all, results]);

      return results;
    },

    // Returns whether or not the current node is an
    // ancestor of the passed node
    // Ancestor:  X is ancestor of Y if:
    //              X is a parent of Y or
    //              X is the ancestor of the parent of Y
    isAncestor: function (node) {
      var isLeftChild, isRightChild;

      if (this.left)  isLeftChild = this.left === this.left.equals(node);
      if (this.right) isRightChild = this.right.equals(node);

      if (isLeftChild || isRightChild) {
        return true;
      }

      return this.left.isAncestor(node) || this.right.isAncestor(node);
    },

    // Ancestor:  X is descendant of Y if:
    //              X is a child of Y or
    //              X is a descendant of a child of Y
    isDescendant: function (node) {

    }
  };
};

