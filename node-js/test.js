if (typeof require !== 'undefined') {
  var Tree = require('./Tree.js').Tree;
}

var l8l1 = Tree(null, null, 8);
var l8l2 = Tree(null, null, 8);
var l8two = Tree(null, null, 8);
var l20two = Tree(l8two, null, 20);
var l10 = Tree(null, null, 10);
var l20 = Tree(l10, l20two, 20);

var l3 = Tree(null, null, 3);
var l3clone = Tree(null, null, 3);

var l8 = Tree(l3, l20, 8);
var l52 = Tree(null, null, 52);
var l30 = Tree(l8, l52, 30);

// console.log(l3.equals(l3clone));
// console.log(l3.isLeaf());

// var results = l30.find(8, true);
// for (var i = 0; i < results.length; i++) {
//   console.log(results[i].valueOf());
// }

if (typeof process !== 'undefined') process.exit(1);

var root = l30.valueOf();

var width = 600,
    height = 700;

var cluster = d3.layout.cluster()
    .size([height, width]);

var diagonal = d3.svg.diagonal()
    .projection(function (d) {
    return [d.y, d.x];
});

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    // .attr("transform", "translate(40,0)")
    .attr("transform", "rotate(90, 300, 350)");
    // .attr("transform", "rotate(0, 90, -180)");

var nodes = cluster.nodes(root),
    links = cluster.links(nodes);

var link = svg.selectAll(".link")
    .data(links)
    .enter().append("path")
    .attr("class", "link")
    .style("stroke-width", function (d) {
    return 1;
})
    .attr("d", diagonal);

var node = svg.selectAll(".node")
    .data(nodes)
    .enter().append("g")
    .attr("class", "node")
    .attr("transform", function (d) {
      return "translate(" + d.y + "," + d.x + ")";
    });

node.append("circle")
    .attr("r", 15);

node.append("text")
    .attr("dx", function (d) {
      // return d.right ? -50 : 20;
      return -7;
    })
    .attr('font-size', '15')
    .attr('transform', 'rotate(270, 0, 0)')
    .attr("dy", 3)

    .text(function (d) {
    return d.value;
});

d3.select(self.frameElement).style("height", height + "px");