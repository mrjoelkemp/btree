"""
Author: 	Joel Kemp
File:		node.py
Purpose: 	Basis for a tree. A tree consists of a root node and left and right children.
			Each child may also have subtrees.
Usage:		from node import *
Note: 		Since this is for educational purposes, the methods
			don't abide by a known tree implementation's API.
"""

class Node:
	def __init__(self, v = 0):
		"""
		Purpose: Constructor with the ability to set a default value
		"""
		self.value = v
		self.left = None
		self.right = None
	
	def add(self, v):
		""" 
		Purpose: Helper to wrap a given value as a node and add it to 
				 the current node's subtree.
		"""
		node = v
		# If we're not getting in a node, then make it one
		if type(v) != "Node":
			node = Node(v)
		self.addChild(node)

	def addChild(self, node):
		"""
		Purpose: 	Adds the passed node as a left or right child based on the node's value
		Precond: 	node should be an instance of Node
		Notes: 		If you don't have children, then add the node based on its value
					Otherwise: 
						If the child is supposed to be your left child, then ask 
							your current left child to accept the node.
						Else, ask your right child to accept the node.
		"""
		# Left child: the node's value is less than the our value
		if(node.value < self.value):
			# if you have a left child, then ask them to add this node
			if self.have_left_child():	
				self.left.addChild(node)
			# otherwise, node is your new left baby
			else: 
				self.left = node

		# Right child: otherwise.
		else:
			# if you already have a right child, then pass the child on
			if self.have_right_child():
				self.right.addChild(node)
			else:
				self.right = node

	def search(self, v):
		"""
		Purpose: 	Search for the value v in the current node's subtree
		Precond:	v is a non-node value
		Returns: 	True if the value exists in the subtree, false otherwise.
		"""
		# Check your own value
		if self.value == v:
			return True

		# If you don't have any children
		if not self.have_left_child() and not self.have_right_child():
			return False

		# Check children
		# If you find the value in either of your children's trees, then we'll get True
		found_left 	= False
		found_right = False
		if self.have_right_child():
			found_left = self.right.search(v)
		if self.have_left_child():
			found_right = self.left.search(v)

		return found_left or found_right

	def get_subtree_for_node(self, v):
		"""
		Returns: A list of subtree values for the node with value v
		"""

		# If my value is v
		if self.value == v: 
			return self.get_subtree_values()
			
		# If I am not the node with the proper value
		else:
			# If you don't have any children,
			# 	there's no way to find the node we're looking for.
			if not self.have_any_children():
				return []

			# Look for the node in your children
			if v < self.value and self.have_left_child():
				return self.left.get_subtree_for_node(v)
			if v > self.value and self.have_right_child():
				return self.right.get_subtree_for_node(v)

	def get_left_subtree_values(self):
		sub = [self.value]
		sub.extend(self.left.get_subtree_values())
		return sub

	def get_right_subtree_values(self):
		sub = [self.value]
		sub.extend(self.right.get_subtree_values())
		return sub
	
	def get_subtree_values(self):
		"""
		Returns: the current node's entire subtree values as a list
		"""
		sub = [self.value]
		if self.have_left_child():
			sub.extend(self.left.get_subtree_values())
		if self.have_right_child():
			sub.extend(self.right.get_subtree_values())

		return sub

	def size(self):
		"""
		Returns the size of the current node's subtree
		"""
		subtree = self.get_subtree_values()
		return len(subtree)

	# PRINTERS		
	def printPath(self, v):
		"""
		Purpose: Prints the path of nodes travelled to find v, if it exists
		"""
		print(self.value)
		
		if self.value == v:
			return
		
		# Choose which child to visit
		if self.have_left_child() and v < self.value:
			self.left.printPath(v)
		if self.have_right_child() and v > self.value:
			self.right.printPath(v)

	def printTree(self, parent_value = None):
		"""
		Purpose: Prints the current node's data and that of its children
		Precond: parent_value keeps track of the current node's parent value for clarity
		"""

		# Print our data
		print(self.value, "\t| Parent =", parent_value)

		# Print left child
		if self.have_left_child():
			print(self.value, "Left")		
			self.left.printTree(self.value)
		else:	
			print(self.value, "No left child")

		# Print right child
		if self.have_right_child():
			print(self.value, "Right")
			self.right.printTree(self.value)
		else:
			print(self.value, "No right child")

	def printRoot(self):
		#TODO: Test
		# If you don't have a parent, then you're the root
		if not self.have_parent():
			print(self.value)
	
	def printLeaves():
		#TODO: Implement
		#If you don't have any children, then you're a leaf
		# Note: a tree with one node has the root = leaf
		return

	# HELPERS
	def have_parent(self):
		"""
		Purpose: Determines whether or not the current node has a parent
		"""

		# We can't just check if you're part of a subtree due to duplicates
		#	dups are in the right subtree
		return

	def flatten(self, x):
		"""
		Purpose: 	Helper to flatten nested lists into a single list
		Returns: 	The flattened list
		Note: 		Code taken from http://stackoverflow.com/questions/5409224/python-recursively-flatten-a-list
		"""
		try:
			it = iter(x)
		except TypeError:
			yield x
		else:
			for i in it:
				for j in flatten(i):
					yield j

	def have_any_children(self):
		"""
		Returns: 	True if you have at least one child. False otherwise.
		"""
		return self.have_left_child() or self.have_right_child()

	def have_left_child(self):
		return self.left != None
	
	def have_right_child(self):
		return self.right != None

	def in_right_subtree(self, v):
		"""
		Purpose: 	Determines if the passed value is a value in the current
					node's right subtree
		Returns: 	True if v is in our right subtree. False otherwise
		"""
		right_values = self.get_right_subtree_values()
		# our_values = self.get_subtree_values()
		right_values = self.flatten(right_values)

		# If the value is not in our entire subtree, 
		#	then it can't in the right subtree
		if not v in right_values:
			return False

		return True
