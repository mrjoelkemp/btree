"""
Author: 	Joel Kemp
File: 		main.py
Purpose: 	Testbed for playing with the custom tree implementation.
"""

from node import *
from random import randint

def main():

	#tree = JTree(50)
	tree = Node(50)
	
	vals = [11, 94, 32, 58, 83]
	for i in vals:
		# tree.addValue(i)
		tree.add(i)

	tree.printTree()
	
	n = 83
	print("\nSearching for", n)
	found = tree.search(n)
	print("Found " + str(n) + " = ", found)
	
	print("\nTree path to", n)
	tree.printPath(n)

	sub_vals = tree.get_subtree_values()
	print("\nTree Vals:", sub_vals)

	lt_vals = tree.get_left_subtree_values()
	print("\nLeft Tree Vals:", lt_vals)
	rt_vals = tree.get_right_subtree_values()
	print("Right Tree Vals:", rt_vals)
	
	k = 94
	n_vals = tree.get_subtree_for_node(k)
	print(k, "subtree =", n_vals)

	#print("Tree size:", tree.size())
main()